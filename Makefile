# **************************************************************************** #
#                                                                              #
#                                                         :::      ::::::::    #
#    Makefile                                           :+:      :+:    :+:    #
#                                                     +:+ +:+         +:+      #
#    By: derkan <derkan@student.42.fr>              +#+  +:+       +#+         #
#                                                 +#+#+#+#+#+   +#+            #
#    Created: 2018/10/09 13:12:28 by derkan            #+#    #+#              #
#    Updated: 2019/03/04 14:43:43 by derkan           ###   ########.fr        #
#                                                                              #
# **************************************************************************** #

NAME=fillit
LIBFT=libft/libft.a
INC=./includes
CC = gcc
CFLAGS = -Wall -Wextra -Werror -I ./libft -I $(INC)
SRC=src/read.c src/read2.c src/solve.c src/solve2.c src/grid.c src/main.c

OBJ = $(SRC:%.c=%.o)

.PHONY: all
all: $(NAME)

# link rule

$(NAME): $(LIBFT) $(OBJ)
	$(CC) $(CFLAGS) $(OBJ) -o $(NAME) -L libft -lft

# object files

solve.o : fillit.h solve.c
	gcc $(CFLAGS) -c solve.c
solve2.o : fillit.h solve2.c
	gcc $(CFLAGS) -c solve2.c
grid.o : fillit.h grid.c
	gcc $(CFLAGS) -c grid.c
read.o : fillit.h read.c
	gcc $(CFLAGS) -c read.c
read2.o : fillit.h read2.c
	gcc $(CFLAGS) -c read2.c
main.o : fillit.h main.c
	gcc $(CFLAGS) -c main.c

# cleaning rules

.PHONY: clean
clean:
	make -C libft clean
	/bin/rm -f $(OBJ)

.PHONY: fclean
fclean: clean
	make -C libft fclean
	/bin/rm -f $(NAME)

.PHONY: re
re: fclean all

$(LIBFT) :
	make -C libft

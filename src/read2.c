/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   read2.c                                            :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: derkan <derkan@student.42.fr>              +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2019/01/15 16:20:58 by derkan            #+#    #+#             */
/*   Updated: 2019/02/09 14:00:21 by derkan           ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "fillit.h"

/*
** return a tetra from a string of 20 characters
*/

t_tetra		get_single_tetra(char *offset)
{
	size_t	i;
	size_t	j;
	t_tetra	t;

	i = 0;
	j = 0;
	while (i < 20)
	{
		if (offset[i] == '#')
		{
			t.pos[j].x = i % 5;
			t.pos[j].y = i / 5;
			++j;
		}
		++i;
	}
	t = shift_points(t);
	return (t);
}

/*
** return an array of tetra, the last tetra has its 'end' field set to 0
*/

t_tetra		*return_array(char *str, t_tetra *array, size_t nbt)
{
	size_t	i;
	size_t	index;
	t_tetra	tmp;

	i = 0;
	while (i < nbt)
	{
		index = i * 21;
		tmp = get_single_tetra(str + index);
		tmp.is_inside = 0;
		tmp.id = 'A' + (char)i;
		tmp.end = 0;
		array[i] = tmp;
		++i;
	}
	tmp.is_inside = 0;
	tmp.id = '*';
	tmp.end = 1;
	array[i] = tmp;
	return (array);
}

t_tetra		*get_all_tetra(char *str, size_t len)
{
	t_tetra	*array;

	array = (t_tetra *)ft_memalloc(27 * sizeof(t_tetra));
	if (!array)
		return (NULL);
	array = return_array(str, array, (len + 1) / 21);
	return (array);
}

t_grid		*new_grid(size_t nbt, t_tetra *ta)
{
	t_grid		*g;

	if ((g = malloc(sizeof(t_grid))) == NULL)
		return (NULL);
	g->dim = 2;
	g->max = 32;
	g->nbt = nbt;
	g->tarray = ta;
	while (g->dim * g->dim < nbt * 4)
		++g->dim;
	if ((g->grid = ft_strnew(g->max * g->max)) == NULL)
	{
		free(g);
		return (NULL);
	}
	ft_memset((void *)g->grid, (int)'.', g->max * g->max);
	return (g);
}

t_grid		*return_input(char *path)
{
	int		fd;
	char	buf[547];
	ssize_t	r;
	t_tetra	*tarray;
	t_grid	*grid;

	if ((fd = open(path, O_RDONLY)) < 0)
		return (NULL);
	if ((r = read(fd, buf, 547)) < 0)
		return (NULL);
	buf[r] = '\0';
	if (r < 20 || r > 545 || (r + 1) % 21 != 0 || buf[r - 1] != '\n'
		|| !check_str(buf, r))
		return (NULL);
	if ((tarray = get_all_tetra(buf, (size_t)ft_strlen(buf))) == NULL)
		return (NULL);
	grid = new_grid((size_t)((r + 1) / 21), tarray);
	if (!grid)
		free(tarray);
	close(fd);
	return (grid);
}

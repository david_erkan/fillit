/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   solve.c                                            :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: derkan <derkan@student.42.fr>              +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2019/01/15 16:20:58 by derkan            #+#    #+#             */
/*   Updated: 2019/02/09 14:01:05 by derkan           ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "fillit.h"

/*
** Checks if g->tarray[index] (of type t_tetra) can be
** placed inside the grid
*/

int			is_free(t_grid *g, size_t index, size_t mod, size_t dv)
{
	size_t	i;
	size_t	offset;
	size_t	off_mod;
	size_t	off_dv;

	i = 0;
	while (i < 4)
	{
		off_mod = mod + g->tarray[index].pos[i].x;
		off_dv = dv + g->tarray[index].pos[i].y;
		offset = (off_dv) * g->max + off_mod;
		if (g->grid[offset] != '.' || off_mod >= g->dim || off_dv >= g->dim)
			return (0);
		++i;
	}
	return (1);
}

/*
** Finds the first empty square starting
**   (*dv) * g->max + (*mod)
*/

int			find_free_pos(t_grid *g, size_t index, size_t *mod, size_t *dv)
{
	size_t i;

	i = (*dv) * g->max + (*mod);
	while (i < g->max * g->max)
	{
		*mod = i % g->max;
		*dv = i / g->max;
		if (*mod < g->dim && *dv < g->dim)
		{
			if (is_free(g, index, *mod, *dv))
				return (1);
		}
		++i;
	}
	return (0);
}

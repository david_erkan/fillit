/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   main.c                                             :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: derkan <derkan@student.42.fr>              +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2019/01/15 16:20:58 by derkan            #+#    #+#             */
/*   Updated: 2019/02/09 13:59:19 by derkan           ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "fillit.h"

int			main(int argc, char **argv)
{
	t_grid	*g;

	if (argc != 2)
	{
		ft_putstr("usage: fillit input_file\n");
		return (1);
	}
	if ((g = return_input(argv[1])) == NULL)
	{
		ft_putstr("error\n");
		return (1);
	}
	solve(g);
	print_grid(g);
	free(g->tarray);
	free(g->grid);
	free(g);
	return (0);
}

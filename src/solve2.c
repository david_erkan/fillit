/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   solve2.c                                           :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: derkan <derkan@student.42.fr>              +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2019/01/15 16:20:58 by derkan            #+#    #+#             */
/*   Updated: 2019/02/09 14:01:47 by derkan           ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "fillit.h"

void		put_inside(t_grid *g, size_t index, size_t mod, size_t dv)
{
	size_t	i;
	size_t	offset;

	i = 0;
	while (i < 4)
	{
		offset = (dv + g->tarray[index].pos[i].y) * g->max
			+ mod + g->tarray[index].pos[i].x;
		g->grid[offset] = g->tarray[index].id;
		++i;
	}
	g->tarray[index].is_inside = 1;
}

void		remove_tetra(t_grid *g, size_t index, size_t mod, size_t dv)
{
	size_t	i;
	size_t	offset;

	i = 0;
	while (i < 4)
	{
		offset = (dv + g->tarray[index].pos[i].y) * g->max
			+ mod + g->tarray[index].pos[i].x;
		g->grid[offset] = '.';
		++i;
	}
	g->tarray[index].is_inside = 0;
}

/*
** Recursive backtracking
** returns 1 if it reaches the ending tetri
** returns 0 if every combination has been tried
*/

int			backtracking(t_grid *g, size_t index)
{
	size_t	mod;
	size_t	dv;
	size_t	i;

	mod = 0;
	dv = 0;
	if (g->tarray[index].end == 1)
		return (1);
	while (find_free_pos(g, index, &mod, &dv))
	{
		put_inside(g, index, mod, dv);
		if (backtracking(g, index + 1))
			return (1);
		if (g->tarray[index].is_inside)
			remove_tetra(g, index, mod, dv);
		i = (dv * g->max + mod) + 1;
		mod = i % g->max;
		dv = i / g->max;
	}
	return (0);
}

/*
** tries to solve
** in case it doesn't work, resizes the grid
*/

void		solve(t_grid *g)
{
	while (!backtracking(g, 0))
		wipe_and_resize(g);
}

/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   grid.c                                             :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: derkan <derkan@student.42.fr>              +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2019/01/15 16:20:58 by derkan            #+#    #+#             */
/*   Updated: 2019/02/09 13:59:09 by derkan           ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "fillit.h"

void		print_grid(t_grid *g)
{
	size_t	i;
	size_t	mod;
	size_t	dv;

	i = 0;
	while (i < g->max * g->max)
	{
		mod = i % g->max;
		dv = i / g->max;
		if (mod < g->dim && dv < g->dim)
		{
			ft_putchar(g->grid[i]);
			if (mod == g->dim - 1)
				ft_putchar('\n');
		}
		++i;
	}
}

void		print_whole_grid(t_grid *g)
{
	size_t	i;
	size_t	mod;

	i = 0;
	while (i < g->max * g->max)
	{
		mod = i % g->max;
		ft_putchar(g->grid[i]);
		if (mod == g->max - 1)
			ft_putchar('\n');
		++i;
	}
}

/*
** Will wipe the grid clean and increase its size
** increasing the size is just a matter of incrementing an integer
** the grid is intentionally bigger than it should and is linked to
** t_grid::max
*/

void		wipe_and_resize(t_grid *g)
{
	ft_memset((void *)g->grid, (int)'.', g->max * g->max);
	++g->dim;
}

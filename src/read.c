/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   read.c                                             :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: derkan <derkan@student.42.fr>              +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2019/01/15 16:20:58 by derkan            #+#    #+#             */
/*   Updated: 2019/02/09 13:59:45 by derkan           ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "fillit.h"

/*
** check if there are only valid chars inside the input :
** each line must end with '\n' and be made of '.' and '#'
*/

int			check_chars(char *offset)
{
	size_t	i;
	size_t	mod;

	if (offset[20] && offset[20] != '\n')
		return (0);
	i = 0;
	while (i < 20)
	{
		mod = (i + 1) % 5;
		if ((!mod && offset[i] != '\n') || !offset[i]
			|| (mod && offset[i] != '.' && offset[i] != '#'))
			return (0);
		++i;
	}
	return (1);
}

/*
** check if the input contains a valid tetra :
** four blocks and the sum of all the adj. squares must be 6 or 8
*/

int			check_adj_squares(char *offset)
{
	int		result;
	int		blocks;
	int		i;

	i = 0;
	result = 0;
	blocks = 0;
	while (i < 20)
	{
		if (offset[i] == '#')
		{
			++blocks;
			if (i - 1 >= 0 && offset[i - 1] == '#')
				++result;
			if (i + 1 < 20 && offset[i + 1] == '#')
				++result;
			if (i - 5 >= 0 && offset[i - 5] == '#')
				++result;
			if (i + 5 < 20 && offset[i + 5] == '#')
				++result;
		}
		++i;
	}
	return (blocks == 4 && (result == 6 || result == 8));
}

/*
** check if the input is valid :
** for each tetra, checks if it has the right ammount of '\n'
** and if each tetra is made of only 4 adjacent blocks
*/

int			check_str(char *str, ssize_t r)
{
	size_t i;
	size_t offset;
	size_t nbt;

	i = 0;
	nbt = (size_t)((r + 1) / 21);
	while (i < nbt)
	{
		offset = i * 21;
		if (!check_chars(str + offset) || !check_adj_squares(str + offset))
			return (0);
		++i;
	}
	return (1);
}

/*
** shift the points so that the leftmost point has t.x == 0
** and the upmost point has t.y == 0
*/

t_tetra		shift_points(t_tetra t)
{
	int		minx;
	int		miny;
	size_t	i;

	i = 0;
	minx = 100;
	miny = 100;
	while (i < 4)
	{
		if (t.pos[i].x < minx)
			minx = t.pos[i].x;
		if (t.pos[i].y < miny)
			miny = t.pos[i].y;
		++i;
	}
	i = 0;
	while (i < 4)
	{
		t.pos[i].x -= minx;
		t.pos[i].y -= miny;
		++i;
	}
	return (t);
}

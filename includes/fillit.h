/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   fillit.h                                           :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: derkan <derkan@student.42.fr>              +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2019/01/15 16:20:45 by derkan            #+#    #+#             */
/*   Updated: 2019/02/09 14:04:16 by derkan           ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#ifndef FILLIT_H
# define FILLIT_H

# include "libft.h"
# include <fcntl.h>
# include <stdlib.h>

typedef struct	s_point
{
	int			x;
	int			y;
}				t_point;

typedef	struct	s_tetra
{
	t_point		pos[4];
	char		id;
	int			is_inside;
	int			end;
}				t_tetra;

typedef struct	s_grid
{
	char		*grid;
	size_t		dim;
	size_t		max;
	size_t		nbt;
	t_tetra		*tarray;
}				t_grid;

/*
** READ
*/

int				check_chars(char *offset);
int				check_adj_squares(char *offset);
int				check_str(char *str, ssize_t r);
t_tetra			shift_points(t_tetra t);

/*
** READ 2
*/

t_tetra			get_single_tetra(char *offset);
t_tetra			*return_array(char *str, t_tetra *array, size_t nbt);
t_tetra			*get_all_tetra(char *str, size_t len);
t_grid			*new_grid(size_t nbt, t_tetra *ta);
t_grid			*return_input(char *path);

/*
** GRID
*/

void			print_grid(t_grid *g);
void			print_whole_grid(t_grid *g);
void			wipe_and_resize(t_grid *g);

/*
** SOLVE
*/

int				is_free(t_grid *g, size_t index, size_t	mod, size_t	dv);
int				find_free_pos(t_grid *g, size_t index, size_t *mod, size_t *dv);

/*
** SOLVE 2
*/

void			put_inside(t_grid *g, size_t index, size_t	mod, size_t	dv);
void			remove_tetra(t_grid *g, size_t index, size_t	mod, size_t	dv);
int				backtracking(t_grid *g, size_t index);
void			solve(t_grid *g);

#endif

/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   ft_strstr.c                                        :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: derkan <derkan@student.42.fr>              +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2018/10/09 13:11:05 by derkan            #+#    #+#             */
/*   Updated: 2018/11/13 15:15:25 by derkan           ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "libft.h"

char	*ft_strstr(const char *haystack, const char *needle)
{
	size_t len;
	size_t i;

	if (!*haystack && !*needle)
		return ((char *)haystack);
	len = ft_strlen(needle);
	i = 0;
	while (haystack[i])
		if (!ft_memcmp(&haystack[i++], needle, len))
			return ((char *)&haystack[i - 1]);
	return (NULL);
}

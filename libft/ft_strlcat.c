/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   ft_strlcat.c                                       :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: derkan <derkan@student.42.fr>              +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2018/10/09 12:58:02 by derkan            #+#    #+#             */
/*   Updated: 2018/10/24 14:15:06 by derkan           ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "libft.h"

size_t		ft_strlcat(char *dst, const char *src, size_t size)
{
	size_t	dl;
	size_t	sl;
	size_t	n;
	size_t	i;

	dl = ft_strlen(dst);
	sl = ft_strlen(src);
	if (size <= dl)
		return (size + sl);
	n = size - dl - 1;
	i = 0;
	while (i < n && i < sl)
	{
		dst[dl + i] = src[i];
		++i;
	}
	dst[dl + i] = '\0';
	return (sl + dl);
}

/*
** #include <stdio.h>
** #include <stdlib.h>
** #include <string.h>
**
** #include <bsd/stdlib.h>
** #include <bsd/string.h>
** #include <bsd/stdio.h>
**
** int		main()
** {
**
** RESULT.LOG
** 	 ----------------- 1
** 	char *str = "the cake is a lie !\0I'm hidden lol\r\n";
** 	char buff1[0xF00] = "there is no stars in the sky";
** 	char buff2[0xF00] = "there is no stars in the sky";
** 	size_t max = strlen("the cake is a lie !\0I'm hidden lol\r\n") + 4;
**
** 	size_t n = strlcat(buff1, str, max);
** 	printf("%s : %lu\n", buff1, n);
**	ft_strlcat(buff2, str, max);
**	if (!strcmp(buff1, buff2))
**		printf("bad\n");
**	else
**		printf("bad\n");
**
**	----------------- 2
**	char *str = "the cake is a lie !\0I'm hidden lol\r\n";
**	char buff1[0xF00] = "there is no stars in the sky";
**	char buff2[0xF00] = "there is no stars in the sky";
**	size_t max = strlen("the cake is a lie !\0I'm hidden lol\r\n") + 4;
**	size_t r1 = strlcat(buff1, str, max);
**	size_t r2 = ft_strlcat(buff2, str, max);
**	printf("%s : %lu\n", buff1, r1);
**	printf("%s : %lu\n", buff2, r2);
**	printf("\n");
**
**	char s1[4] = "";
**	char s2[4] = "";
**	r1 = strlcat(s1, "thx to ntoniolo for this test !", 4);
**	r2 = ft_strlcat(s2, "thx to ntoniolo for this test !", 4);
**	printf("%s : %lu\n", s1, r1);
**	printf("%s : %lu\n", s2, r2);
**	printf("\n");
**
**	// -------------------3
**	char *str3 = "the cake is a lie !\0I'm hidden lol\r\n";
**	char buff13[0xF00] = "there is no stars in the sky";
**	char buff23[0xF00] = "there is no stars in the sky";
**	size_t max3 = strlen("the cake is a lie !\0I'm hidden lol\r\n")
**		+ strlen("there is no stars in the sky");
**
**	size_t r13 =  strlcat(buff13, str3, max3);
**	printf("%s : %lu\n", buff13, r13);
**	size_t r23 = ft_strlcat(buff23, str3, max3);
**	printf("%s : %lu\n", buff23, r23);
**	printf("\n");
**
**	// ------------------6
**	char *str6 = "";
**	char buff16[0xF00] = "there is no stars in the sky";
**	char buff26[0xF00] = "there is no stars in the sky";
**	size_t max6 = strlen("there is no stars in the sky") + 1;
**
**	size_t r16 = strlcat(buff16, str6, max6);
**	printf("%s : %lu\n", buff16, r16);
**	size_t r26 = ft_strlcat(buff26, str6, max6);
**	printf("%s : %lu\n", buff26, r26);
**	printf("\n");
**
**	//------------ 9
**	char *src9 = "aaa";
**	char dst19[20];
**	char dst29[20];
**	size_t ret19;
**	size_t ret29;
**
**	memset(dst19, 'B', sizeof(dst19));
**	memset(dst29, 'B', sizeof(dst29));
**	strlcat(dst19, src9, 20);
**	strlcat(dst19, src9, 20);
**	ret19 = strlcat(dst19, src9, 20);
**	ft_strlcat(dst29, src9, 20);
**	ft_strlcat(dst29, src9, 20);
**	ret29 = ft_strlcat(dst29, src9, 20);
**
**	printf("%s : %lu\n", dst19, ret19);
**	printf("%s : %lu\n", dst29, ret29);
**	printf("\n");
**
**
**    int i;
**    for (i = 0; i <= 15; i++)
**    {
**        char dst[15] = "xxx";
**        char src[] = "hello";
**        size_t size;
**        size = strlcat(dst, src, i);
**        char dst2[15] = "xxx";
**        size_t size2;
**        size2 = strlcat(dst2, src, i);
**        printf("[%d] ", i);
**        if (i < 10)
**            printf(" ");
**        printf("dst : %s", dst);
**        // for (int j = 0; j < 8 - (int) size; ++j)
**        // 	printf(" ");
**        printf(" | ");
**        printf("src : %s | ", src);
**        printf("size : %lu\n", size);
**		printf("[%d] ", i);
**        if (i < 10)
**            printf(" ");
**        printf("dst : %s", dst2);
**        // for (int j = 0; j < 8 - (int) size2; ++j)
**        // 	printf(" ");
**        printf(" | ");
**        printf("src : %s | ", src);
**        printf("size : %lu | ", size2);
**                printf("%s\n", !ft_strcmp(dst, dst2) ? "[OK]" : "[KO]");
**
**    }
**    printf("--------------------\n");
**    for (i = 0; i <= 15; i++)
**    {
**        char dst[15] = "";
**        char src[] = "hello";
**        size_t size;
**        size = ft_strlcat(dst, src, i);
**        char dst2[15] = "";
**        size_t size2;
**        size2 = strlcat(dst2, src, i);
**        printf("[%d] ", i);
**        if (i < 10)
**            printf(" ");
**        printf("dst : %s", dst);
**        // for (int j = 0; j < 8 - (int) size; ++j)
**        // 	printf(" ");
**        printf(" | ");
**        printf("src : %s | ", src);
**        printf("size : %lu\n", size);
**		printf("[%d] ", i);
**        if (i < 10)
**            printf(" ");
**        printf("dst : %s", dst2);
**        // for (int j = 0; j < 8 - (int) size2; ++j)
**        // 	printf(" ");
**        printf(" | ");
**        printf("src : %s | ", src);
**        printf("size : %lu | ", size2);
**                printf("%s\n", !ft_strcmp(dst, dst2) ? "[OK]" : "[KO]");
**
**    }
**    printf("--------------------\n");
**    for (i = 0; i <= 15; i++)
**    {
**        char dst[15] = "xxx";
**        char src[] = "hello";
**        size_t size;
**        size = strlcat(dst, src, i);
**        char dst2[15] = "xxx";
**        size_t size2;
**        size2 = strlcat(dst2, src, i);
**        printf("[%d] ", i);
**        if (i < 10)
**            printf(" ");
**        printf("dst : %s", dst);
**        // for (int j = 0; j < 8 - (int) size; ++j)
**        // 	printf(" ");
**        printf(" | ");
**        printf("src : %s | ", src);
**        printf("size : %lu\n", size);
**		printf("[%d] ", i);
**        if (i < 10)
**            printf(" ");
**        printf("dst : %s", dst2);
**        // for (int j = 0; j < 8 - (int) size2; ++j)
**        // 	printf(" ");
**        printf(" | ");
**        printf("src : %s | ", src);
**        printf("size : %lu | ", size2);
**                printf("%s\n", !ft_strcmp(dst, dst2) ? "[OK]" : "[KO]");
**    }
**    for (i = 0; i <= 15; i++)
**    {
**        char dst[15] = "";
**        char src[] = "hello";
**        size_t size;
**        size = strlcat(dst, src, i);
**        char dst2[15] = "";
**        size_t size2;
**        size2 = strlcat(dst2, src, i);
**        printf("[%d] ", i);
**        if (i < 10)
**            printf(" ");
**        printf("dst : %s", dst);
**        // for (int j = 0; j < 8 - (int) size; ++j)
**        // 	printf(" ");
**        printf(" | ");
**        printf("src : %s | ", src);
**        printf("size : %lu\n", size);
**		printf("[%d] ", i);
**        if (i < 10)
**            printf(" ");
**        printf("dst : %s", dst2);
**        // for (int j = 0; j < 8 - (int) size2; ++j)
**        // 	printf(" ");
**        printf(" | ");
**        printf("src : %s | ", src);
**        printf("size : %lu | ", size2);
**        printf("%s\n", !ft_strcmp(dst, dst2) ? "[OK]" : "[KO]");
**  char test_source[50] = "salut";
**  char test_dest[50] = "salut";
**  ft_strlcat(test_dest, test_source, 5);
**  printf("%s\n", test_dest);
**  return 0;
**    }
**	return 0;
** }
*/

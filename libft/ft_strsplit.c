/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   ft_strsplit.c                                      :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: derkan <derkan@student.42.fr>              +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2018/10/09 13:04:22 by derkan            #+#    #+#             */
/*   Updated: 2019/01/14 18:19:26 by derkan           ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "libft.h"

static char			**allocate_tab(size_t size)
{
	char	**tab;
	size_t	i;

	tab = (char **)malloc(sizeof(char *) * (size + 1));
	if (tab)
		tab[size] = NULL;
	i = 0;
	while (i < size)
		tab[i++] = NULL;
	return (tab);
}

static size_t		nb_split(char const *s, char c)
{
	char	*it;
	size_t	nb_split;

	if (!*s)
		return (0);
	it = (char *)s;
	nb_split = 0;
	while (*it)
	{
		if (it[0] != c && (it[1] == c || !it[1]))
			++nb_split;
		++it;
	}
	return (nb_split);
}

static char			*get_sub(char *start, char *end)
{
	size_t	size;
	char	*sub;

	size = end - start;
	sub = ft_strsub(start, 0, size);
	return (sub);
}

static void			*free_tab(char **tab, size_t size)
{
	size_t i;

	if (!tab)
		return (NULL);
	i = 0;
	while (i < size)
	{
		if (tab[i])
			free(tab[i]);
		++i;
	}
	free(tab);
	return (NULL);
}

char				**ft_strsplit(char const *s, char c)
{
	int		i;
	char	*it;
	char	**tab;
	char	*start;

	if (!s || (tab = allocate_tab(nb_split(s, c))) == NULL)
		return (NULL);
	i = 0;
	it = (char *)s;
	start = NULL;
	while (*it)
	{
		if (it[0] != c && start == NULL)
			start = it;
		if (start && (it[1] == c || it[1] == '\0'))
		{
			if ((tab[i++] = get_sub(start, it + 1)) == NULL)
				return (free_tab(tab, nb_split(s, c)));
			start = NULL;
		}
		++it;
	}
	return (tab);
}

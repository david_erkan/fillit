/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   ft_memmove.c                                       :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: derkan <derkan@student.42.fr>              +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2018/10/09 12:46:04 by derkan            #+#    #+#             */
/*   Updated: 2018/10/24 16:48:03 by derkan           ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "libft.h"

void	*ft_memmove(void *dest, const void *src, size_t n)
{
	t_byte			*id;
	const t_byte	*is;

	id = (t_byte *)dest;
	is = (const t_byte *)src;
	if (id < is)
		ft_memcpy(dest, (const void*)src, n);
	else
	{
		while (n)
		{
			id[n - 1] = is[n - 1];
			--n;
		}
	}
	return (dest);
}

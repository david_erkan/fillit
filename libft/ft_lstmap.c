/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   ft_lstmap.c                                        :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: derkan <derkan@student.42.fr>              +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2018/10/09 13:02:51 by derkan            #+#    #+#             */
/*   Updated: 2018/10/24 14:08:44 by derkan           ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "libft.h"

t_list		*ft_lstmap(t_list *lst, t_list *(*f)(t_list *elem))
{
	t_list *map;

	if (!lst || !f)
		return (NULL);
	map = f(lst);
	lst = lst->next;
	while (lst)
	{
		ft_lst_push_back(&map, f(lst));
		lst = lst->next;
	}
	return (map);
}

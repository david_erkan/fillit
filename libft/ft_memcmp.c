/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   ft_memcmp.c                                        :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: derkan <derkan@student.42.fr>              +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2018/10/09 12:37:00 by derkan            #+#    #+#             */
/*   Updated: 2018/10/24 14:09:48 by derkan           ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "libft.h"

int		ft_memcmp(const void *s1, const void *s2, size_t n)
{
	const t_byte	*it1;
	const t_byte	*it2;

	if (n == 0)
		return (0);
	it1 = (const t_byte *)s1;
	it2 = (const t_byte *)s2;
	while (n--)
	{
		if (*it1 != *it2)
			return ((int)*it1 - (int)*it2);
		++it1;
		++it2;
	}
	return (0);
}

/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   ft_memcpy.c                                        :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: derkan <derkan@student.42.fr>              +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2018/10/09 12:37:00 by derkan            #+#    #+#             */
/*   Updated: 2018/10/24 14:09:54 by derkan           ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "libft.h"

void	*ft_memcpy(void *dest, const void *src, size_t n)
{
	size_t			i;
	const t_byte	*isrc;
	t_byte			*idest;

	isrc = (const t_byte *)src;
	idest = (t_byte *)dest;
	i = 0;
	while (i < n)
	{
		idest[i] = isrc[i];
		++i;
	}
	return (dest);
}

/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   ft_memchr.c                                        :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: derkan <derkan@student.42.fr>              +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2018/10/09 12:37:00 by derkan            #+#    #+#             */
/*   Updated: 2018/10/24 14:09:14 by derkan           ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "libft.h"

void	*ft_memchr(const void *s, int c, size_t n)
{
	const t_byte	*it;
	t_byte			v;
	size_t			i;

	i = 0;
	v = (t_byte)c;
	it = (const t_byte *)s;
	while (i++ < n)
	{
		if (*it == v)
			return ((void *)it);
		++it;
	}
	return (NULL);
}

/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   ft_lstmap.c                                        :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: derkan <derkan@student.42.fr>              +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2018/10/09 13:02:51 by derkan            #+#    #+#             */
/*   Updated: 2018/10/24 14:08:44 by derkan           ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "libft.h"

void		ft_lst_push_back(t_list **alst, t_list *elem)
{
	t_list	*lst;

	if (!alst || !elem)
		return ;
	if (!*alst)
	{
		*alst = elem;
		return ;
	}
	lst = *alst;
	while (lst->next)
		lst = lst->next;
	lst->next = elem;
}

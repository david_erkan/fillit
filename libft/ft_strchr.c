/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   ft_strchr.c                                        :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: derkan <derkan@student.42.fr>              +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2018/10/09 12:48:58 by derkan            #+#    #+#             */
/*   Updated: 2018/11/13 15:22:17 by derkan           ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "libft.h"

char	*ft_strchr(const char *s, int c)
{
	size_t	i;
	char	v;

	i = 0;
	v = (char)c;
	while (s[i])
	{
		if (s[i] == v)
			return ((char *)(s + i));
		++i;
	}
	if (s[i] == v)
		return ((char *)(s + i));
	return (NULL);
}

/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   ft_strtrim.c                                       :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: derkan <derkan@student.42.fr>              +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2018/10/09 13:12:01 by derkan            #+#    #+#             */
/*   Updated: 2018/10/24 14:19:31 by derkan           ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "libft.h"

char	*ft_strtrim(char const *s)
{
	size_t	len;
	size_t	st;
	size_t	end;

	if (s == NULL)
		return (NULL);
	len = ft_strlen(s);
	end = len - 1;
	st = 0;
	while ((s[st] == '\n' || s[st] == '\t' || s[st] == ' ') && s[st])
		++st;
	if (st == len)
		return (ft_strdup(""));
	while ((s[end] == '\n' || s[end] == '\t' || s[end] == ' ') && end != 0)
		--end;
	return (ft_strsub(s, st, end - st + 1));
}

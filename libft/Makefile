# **************************************************************************** #
#                                                                              #
#                                                         :::      ::::::::    #
#    Makefile                                           :+:      :+:    :+:    #
#                                                     +:+ +:+         +:+      #
#    By: derkan <derkan@student.42.fr>              +#+  +:+       +#+         #
#                                                 +#+#+#+#+#+   +#+            #
#    Created: 2018/10/09 13:12:28 by derkan            #+#    #+#              #
#    Updated: 2018/12/05 14:12:29 by derkan           ###   ########.fr        #
#                                                                              #
# **************************************************************************** #

NAME=libft.a
INC=./
CC = gcc
CFLAGS = -Wall -Wextra -Werror -I ./
SRC=ft_memset.c ft_bzero.c ft_memcpy.c ft_memccpy.c ft_memmove.c ft_memchr.c \
ft_memcmp.c ft_strlen.c ft_strdup.c ft_strcpy.c ft_strncpy.c ft_strcat.c \
ft_strncat.c ft_strlcat.c ft_strchr.c ft_strrchr.c ft_strstr.c ft_strnstr.c \
ft_strcmp.c ft_strncmp.c ft_atoi.c ft_isalpha.c ft_isdigit.c ft_isalnum.c \
ft_isascii.c ft_isprint.c ft_toupper.c ft_tolower.c ft_memalloc.c ft_putchar.c \
ft_putstr.c ft_putendl.c ft_putnbr.c ft_putchar_fd.c ft_putstr_fd.c \
ft_putendl_fd.c ft_putnbr_fd.c ft_memdel.c ft_strnew.c ft_strdel.c ft_strclr.c \
ft_striter.c ft_striteri.c ft_strmap.c ft_strmapi.c ft_strequ.c ft_strnequ.c \
ft_strsub.c ft_strjoin.c ft_strtrim.c ft_strsplit.c ft_itoa.c \
ft_lstadd.c ft_lstdel.c ft_lstdelone.c ft_lstiter.c ft_lstmap.c ft_lstnew.c \
ft_lst_push_back.c ft_strjoin_del.c ft_strndup.c

# version Thomas
OBJ = $(SRC:%.c=%.o)

# version moi
# OBJ=ft_memset.o ft_bzero.o ft_memcpy.o ft_memccpy.o ft_memmove.o ft_memchr.o \
# ft_memcmp.o ft_strlen.o ft_strdup.o ft_strcpy.o ft_strncpy.o ft_strcat.o \
# ft_strncat.o ft_strlcat.o ft_strchr.o ft_strrchr.o ft_strstr.o ft_strnstr.o \
# ft_strcmp.o ft_strncmp.o ft_atoi.o ft_isalpha.o ft_isdigit.o ft_isalnum.o \
# ft_isascii.o ft_isprint.o ft_toupper.o ft_tolower.o ft_memalloc.o ft_putchar.o \
# ft_putstr.o ft_putendl.o ft_putnbr.o ft_putchar_fd.o ft_putstr_fd.o \
# ft_putendl_fd.o ft_putnbr_fd.o ft_memdel.o ft_strnew.o ft_strdel.o ft_strclr.o \
# ft_striter.o ft_striteri.o ft_strmap.o ft_strmapi.o ft_strequ.o ft_strnequ.o \
# ft_strsub.o ft_strjoin.o ft_strtrim.o ft_strsplit.o ft_itoa.o \
# ft_lstadd.o ft_lstdel.o ft_lstdelone.o ft_lstiter.o ft_lstmap.o ft_lstnew.o \
# ft_lst_push_back.o ft_strjoin_del.c ft_strndup.c

.PHONY: all
all: $(OBJ) $(NAME)

$(NAME): $(OBJ)
	ar rc $(NAME) $(OBJ)
	ranlib $(NAME)

.PHONY: clean
clean:
	/bin/rm -f $(OBJ)

.PHONY: fclean
fclean: clean
	/bin/rm -f $(NAME)

.PHONY: re
re: fclean all

# .PHONY: all clean fclean re so

# shared library rule
# .PHONY: so
# so:
# 	gcc -c $(CFLAGS)  -fpic $(SRC)
# 	gcc -shared -o libft.so *.o

ft_memset.o : libft.h ft_memset.c
	gcc $(CFLAGS) -c ft_memset.c
ft_bzero.o : libft.h ft_bzero.c
	gcc $(CFLAGS) -c ft_bzero.c
ft_memcpy.o : libft.h ft_memcpy.c
	gcc $(CFLAGS) -c ft_memcpy.c
ft_memccpy.o : libft.h ft_memccpy.c
	gcc $(CFLAGS) -c ft_memccpy.c
ft_memmove.o : libft.h ft_memmove.c
	gcc $(CFLAGS) -c ft_memmove.c
ft_memchr.o : libft.h ft_memchr.c
	gcc $(CFLAGS) -c ft_memchr.c
ft_memcmp.o : libft.h ft_memcmp.c
	gcc $(CFLAGS) -c ft_memcmp.c
ft_strlen.o : libft.h ft_strlen.c
	gcc $(CFLAGS) -c ft_strlen.c
ft_strdup.o : libft.h ft_strdup.c
	gcc $(CFLAGS) -c ft_strdup.c
ft_strcpy.o : libft.h ft_strcpy.c
	gcc $(CFLAGS) -c ft_strcpy.c
ft_strncpy.o : libft.h ft_strncpy.c
	gcc $(CFLAGS) -c ft_strncpy.c
ft_strcat.o : libft.h ft_strcat.c
	gcc $(CFLAGS) -c ft_strcat.c
ft_strncat.o : libft.h ft_strncat.c
	gcc $(CFLAGS) -c ft_strncat.c
ft_strlcat.o : libft.h ft_strlcat.c
	gcc $(CFLAGS) -c ft_strlcat.c
ft_strchr.o : libft.h ft_strchr.c
	gcc $(CFLAGS) -c ft_strchr.c
ft_strrchr.o : libft.h ft_strrchr.c
	gcc $(CFLAGS) -c ft_strrchr.c
ft_strstr.o : libft.h ft_strstr.c
	gcc $(CFLAGS) -c ft_strstr.c
ft_strnstr.o : libft.h ft_strnstr.c
	gcc $(CFLAGS) -c ft_strnstr.c
ft_strcmp.o : libft.h ft_strcmp.c
	gcc $(CFLAGS) -c ft_strcmp.c
ft_strncmp.o : libft.h ft_strncmp.c
	gcc $(CFLAGS) -c ft_strncmp.c
ft_atoi.o : libft.h ft_atoi.c
	gcc $(CFLAGS) -c ft_atoi.c
ft_isalpha.o :  libft.h ft_isalpha.c
	gcc $(CFLAGS) -c ft_isalpha.c
ft_isdigit.o :  libft.h ft_isdigit.c
	gcc $(CFLAGS) -c ft_isdigit.c
ft_isalnum.o : libft.h ft_isalnum.c
	gcc $(CFLAGS) -c ft_isalnum.c
ft_isascii.o :  libft.h ft_isascii.c
	gcc $(CFLAGS) -c ft_isascii.c
ft_isprint.o :  libft.h ft_isprint.c
	gcc $(CFLAGS) -c ft_isprint.c
ft_toupper.o :  libft.h ft_toupper.c
	gcc $(CFLAGS) -c ft_toupper.c
ft_tolower.o :  libft.h ft_tolower.c
	gcc $(CFLAGS) -c ft_tolower.c
ft_memalloc.o : libft.h ft_memalloc.c
	gcc $(CFLAGS) -c ft_memalloc.c
ft_putchar.o : libft.h ft_putchar.c
	gcc $(CFLAGS) -c ft_putchar.c
ft_putstr.o : libft.h ft_putstr.c
	gcc $(CFLAGS) -c ft_putstr.c
ft_putendl.o : libft.h ft_putendl.c
	gcc $(CFLAGS) -c ft_putendl.c
ft_putnbr.o : libft.h ft_putnbr.c
	gcc $(CFLAGS) -c ft_putnbr.c
ft_putchar_fd.o : libft.h ft_putchar_fd.c
	gcc $(CFLAGS) -c ft_putchar_fd.c
ft_putstr_fd.o : libft.h ft_putstr_fd.c
	gcc $(CFLAGS) -c ft_putstr_fd.c
ft_putendl_fd.o : libft.h ft_putendl_fd.c
	gcc $(CFLAGS) -c ft_putendl_fd.c
ft_putnbr_fd.o : libft.h ft_putnbr_fd.c
	gcc $(CFLAGS) -c ft_putnbr_fd.c
ft_memdel.o : libft.h ft_memdel.c
	gcc $(CFLAGS) -c ft_memdel.c
ft_strnew.o : libft.h ft_strnew.c
	gcc $(CFLAGS) -c ft_strnew.c
ft_strdel.o : libft.h ft_strdel.c
	gcc $(CFLAGS) -c ft_strdel.c
ft_strclr.o :  libft.h ft_strclr.c
	gcc $(CFLAGS) -c ft_strclr.c
ft_striter.o :  libft.h ft_striter.c
	gcc $(CFLAGS) -c ft_striter.c
ft_striteri.o : libft.h ft_striteri.c
	gcc $(CFLAGS) -c ft_striteri.c
ft_strmap.o : libft.h ft_strmap.c
	gcc $(CFLAGS) -c ft_strmap.c
ft_strmapi.o : libft.h ft_strmapi.c
	gcc $(CFLAGS) -c ft_strmapi.c
ft_strequ.o : libft.h ft_strequ.c
	gcc $(CFLAGS) -c ft_strequ.c
ft_strnequ.o : libft.h ft_strnequ.c
	gcc $(CFLAGS) -c ft_strnequ.c
ft_strsub.o : libft.h ft_strsub.c
	gcc $(CFLAGS) -c ft_strsub.c
ft_strjoin.o : libft.h ft_strjoin.c
	gcc $(CFLAGS) -c ft_strjoin.c
ft_strtrim.o : libft.h ft_strtrim.c
	gcc $(CFLAGS) -c ft_strtrim.c
ft_strsplit.o :  libft.h ft_strsplit.c
	gcc $(CFLAGS) -c ft_strsplit.c
ft_itoa.o : libft.h ft_itoa.c
	gcc $(CFLAGS) -c ft_itoa.c
ft_lstadd.o : libft.h ft_lstadd.c
	gcc $(CFLAGS) -c ft_lstadd.c
ft_lstdel.o : libft.h ft_lstdel.c
	gcc $(CFLAGS) -c ft_lstdel.c
ft_lstdelone.o : libft.h ft_lstdelone.c
	gcc $(CFLAGS) -c ft_lstdelone.c
ft_lstiter.o : libft.h ft_lstiter.c
	gcc $(CFLAGS) -c ft_lstiter.c
ft_lstmap.o : libft.h ft_lstmap.c
	gcc $(CFLAGS) -c ft_lstmap.c
ft_lstnew.o : libft.h ft_lstnew.c
	gcc $(CFLAGS) -c ft_lstnew.c
ft_lst_push_back.o : libft.h ft_lst_push_back.c
	gcc $(CFLAGS) -c ft_lst_push_back.c
# BONUS PART 2
ft_strjoin_del.o : libft.h ft_strjoin_del.c
	gcc $(CFLAGS) -c ft_strjoin_del.c
ft_strndup.o : libft.h ft_strndup.c
	gcc $(CFLAGS) -c ft_strndup.c
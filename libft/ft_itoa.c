/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   ft_itoa.c                                          :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: derkan <derkan@student.42.fr>              +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2018/10/09 12:37:00 by derkan            #+#    #+#             */
/*   Updated: 2018/10/24 14:14:45 by derkan           ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "libft.h"

static size_t	get_length(int n)
{
	size_t	size;

	if (n == -2147483648)
		return (11);
	size = n < 0 ? 1 : 0;
	n = n < 0 ? -n : n;
	while (n > 0)
	{
		n /= 10;
		++size;
	}
	return (size);
}

char			*ft_itoa(int n)
{
	size_t	size;
	char	*str;

	if (n == 0)
		return (ft_strdup("0"));
	size = get_length(n);
	if ((str = ft_strnew(size)) == NULL)
		return (NULL);
	str[0] = n < 0 ? '-' : '\0';
	str[10] = (n == -2147483648) ? '8' : '\0';
	size = (n == -2147483648) ? size - 1 : size;
	n = (n == -2147483648) ? n / 10 : n;
	n = (n < 0) ? -n : n;
	while (n > 0)
	{
		str[--size] = (char)(n % 10 + 48);
		n /= 10;
	}
	return (str);
}

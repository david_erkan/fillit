/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   ft_memccpy.c                                       :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: derkan <derkan@student.42.fr>              +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2018/10/09 12:37:00 by derkan            #+#    #+#             */
/*   Updated: 2018/12/04 15:37:22 by derkan           ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "libft.h"

void	*ft_memccpy(void *dest, const void *src, int c, size_t n)
{
	size_t			i;
	const t_byte	*isrc;
	t_byte			*idest;

	isrc = (const t_byte*)src;
	idest = (t_byte*)dest;
	i = 0;
	while (i < n)
	{
		idest[i] = isrc[i];
		if (isrc[i++] == (unsigned char)c)
			return ((char*)idest + i);
	}
	return ((void *)NULL);
}

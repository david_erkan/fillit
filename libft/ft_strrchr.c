/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   ft_strrchr.c                                       :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: derkan <derkan@student.42.fr>              +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2018/10/09 13:04:22 by derkan            #+#    #+#             */
/*   Updated: 2018/11/13 15:22:34 by derkan           ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "libft.h"

char	*ft_strrchr(const char *s, int c)
{
	size_t	i;
	char	v;

	v = (char)c;
	i = 0;
	while (s[i])
		++i;
	while (i > 0)
	{
		if (s[i] == v)
			return ((char *)(s + i));
		--i;
	}
	if (s[i] == v)
		return ((char *)(s + i));
	return (NULL);
}

/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   ft_strncmp.c                                       :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: derkan <derkan@student.42.fr>              +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2018/10/09 12:59:26 by derkan            #+#    #+#             */
/*   Updated: 2018/12/04 15:39:19 by derkan           ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "libft.h"

int		ft_strncmp(const char *s1, const char *s2, size_t n)
{
	const t_byte *p1 = (const t_byte *)s1;
	const t_byte *p2 = (const t_byte *)s2;

	while (*p1 && *p1 == *p2 && n--)
	{
		++p1;
		++p2;
	}
	return (n == 0 ? 0 : *p1 - *p2);
}
